#!/usr/bin/env python3

import os
import re
import sys
from datetime import date, datetime, timezone, timedelta
import logging
from filecmp import cmp as filecmp
#import curses
#import curses.textpad
#import cmd
from functools import partial
import pyudev

from prompt_toolkit.shortcuts import button_dialog, prompt, PromptSession
from prompt_toolkit.application import Application
from prompt_toolkit.buffer import Buffer
from prompt_toolkit.key_binding import KeyBindings
from prompt_toolkit.layout.containers import VSplit, HSplit, Window, FloatContainer, Float, DynamicContainer, is_container, ConditionalContainer
from prompt_toolkit.formatted_text import to_formatted_text, Template, is_formatted_text
from prompt_toolkit.layout.controls import FormattedTextControl, BufferControl, UIControl
from prompt_toolkit.layout.layout import Layout
from prompt_toolkit.key_binding.key_bindings import merge_key_bindings
from prompt_toolkit.widgets import TextArea, Dialog, Label, Button, Box, Shadow, Frame
from prompt_toolkit.widgets.base import Border
from prompt_toolkit.eventloop import Future, ensure_future, Return, From
from prompt_toolkit.application.current import get_app
from prompt_toolkit.layout.dimension import Dimension as D, is_dimension, to_dimension
from prompt_toolkit.filters import Condition
from prompt_toolkit.key_binding.bindings import named_commands

from puzzle import *
from util import stardate, open


class PuzzleZeroStage(PuzzleStage):
    auth_path = 'bsu.crt'
    def authed(self, path):
        try:
            if filecmp(os.path.join(path, self.auth_path), self.auth_path):
                return True
        except OSError:
            pass
        return False

class Message:
    def __init__(self, date, subject, body=''):
        self.date = date
        self.subject = subject
        self.body = body


class ObtainOrders(PuzzleZeroStage):
    order_file = 'orders'
    messages = (
        Message(stardate(datetime(2018, 8, 20, 7, 51)), 'EXXXPAND UR MEMBERSHIP TODAY!', 'Do u need an eXXXpansion in ur pance? Are you nable to satisfie youre robot girlfriend? Visit http://totallylegitpeepeepills.co.ru/ today!'),
        Message(stardate(datetime(2018, 8, 23, 8, 0, 0)), 'BSU Authorisation Expired', 'Technician I. Hunter:\nYour BSU authorisation expires today. You will need to insert it into a terminal and run the auth sequence before it can be used. Please do this promptly so that your work is not interrupted.'),
        Message(stardate(datetime(2018, 8, 21, 8, 1)), 'Tetrahedron practice?', 'Hey Hunter are you coming to tetrahedron practice tonight? Sanders can\'t so if you\'re busy as well we have to cancel.\n -- Bucky'),
        Message(stardate(datetime(2018, 8, 18, 11, 4, 53)), 'Retrieving orders', "Don't forget that as of tomorrow you won't be able to retrieve orders without authorising your BSU at the terminal."),
        Message(stardate(datetime(2018, 8, 2, 13, 37)), 'Filter alteration request: denied', 'Technician I. Hunter:\nYour request for a porn filter exemption ref. `PFR.1829/BIG.ANIME.TIDDIES/4a\' was denied by your commanding officer. As you have already appealed porn filter decisions 4 times in the preceding year, no appeal is possible.')
    )
    unmount_immediately = False
    def __init__(self):
        super().__init__('P0.1: Press Any Key')

        self.commands = [f[4:] for f in dir(self) if f.startswith('cmd_') and callable(getattr(self, f)) and getattr(self, f).__doc__ != 'admin']
        self.bsu_checked = False
        self.mountpoint = None

        # TODO this should only disable the console handler, not the file handler, once that's set up.
        # or maybe we only have a file handler in the end...
        logging.getLogger().disabled = True
        self.unplug_monitor = UnplugMonitor('block', self.reset) # Reset on BSU removal
        self.unplug_monitor = UnplugMonitor('input', self.reset) # Reset on keyboard removal

        self.setup_tui()
        self.application.run()

        logging.getLogger().disabled = False

    def reset(self):
        #self.application.exit()
        #del self.application
        #self.setup_tui()
        #self.application.run()
        self.line_start_pos = 0
        self.pak.reset()
        body = FloatContainer(
                content=self.cli_frame,
                key_bindings=self.text_kb,
                floats=[self.dialog_float]
        )
        self.body = body
        self.cli.text = ''
        layout = Layout(body, focused_element=self.pak)
        self.application.layout = layout
        self.application.invalidate()

    def setup_tui(self):
        self.text_kb = KeyBindings()

        @self.text_kb.add('enter')
        def _(event):
            input = self.cli.text[self.line_start_pos:]
            try:
                output = self.handle_line(input)
            except Exception as e:
                output = 'Unexpected Error: ' + str(e)
            if output:
                self.cli.text = self.cli.text + '\n' + output + '\n' + prompt
            else:
                self.cli.text = self.cli.text + '\n' + prompt
            self.line_start_pos = len(self.cli.text)
            self.cli.buffer.cursor_position = self.line_start_pos

        @self.text_kb.add('up')
        def _(event):
            event.current_buffer.history_backward()
        @self.text_kb.add('down')
        def _(event):
            event.current_buffer.history_forward()

        @self.text_kb.add('backspace')
        def _(event):
            if self.cli.buffer.cursor_position > self.line_start_pos:
                named_commands.get_by_name('backward-delete-char')(event)
        @self.text_kb.add('c-w')
        def _(event):
            if self.cli.buffer.cursor_position > self.line_start_pos:
                named_commands.get_by_name('unix-word-rubout')(event)

        prompt = '> '
        self.line_start_pos = 0
        self.cli = TextArea(prompt=prompt)
        self.pak = PressAnyKey(self)
        self.dialog_float = Float(content=self.pak)
        self.cli_frame = MyFrame(self.cli, title='ADMIN COORDINATION TERMINAL 20', width=None)

        body = FloatContainer(
                content=self.cli_frame,
                key_bindings=self.text_kb,
                floats=[self.dialog_float]
        )
        self.body = body
        layout = Layout(body, focused_element=self.pak)

        kb = KeyBindings()

        @kb.add('c-c')
        def _(event):
            " Quit application. "
            self.reset()

        self.application = Application(
            layout=layout,
            key_bindings=kb,
            full_screen=True)
    
    def handle_line(self, input):
        if input == '':
            return ''

        words = input.split(' ')
        cmd_name = words[0]
        try:
            cmd = getattr(self, 'cmd_%s' % cmd_name)
        except AttributeError:
            return 'Command not found: ' + cmd_name

        return cmd(words[1:])

    def cmd_help(self, params):
        """If you need help on how to use the help then, well. You need *help*."""
        if params:
            cmd_name = params[0]
            try:
                cmd = getattr(self, 'cmd_%s' % cmd_name)
            except AttributeError:
                return 'help: no help topics match `%s\'. Try `help\' for a list of commands.' % (cmd_name)
            text = cmd.__doc__
            if not text:
                return 'help: command `%s\' has no documentation. Try smashing the keyboard with your face.' % cmd_name
            return text

        return """\
IHunt Shell version 63.0.0-pre-rc7742
The following shell commands are defined. Type `help <name>' to get help
on a particular command.
  """ + '\n  '.join([cmd for cmd in self.commands])

    def cmd_gtos(self, params):
        """Retrieve outstanding orders"""
        msg = "Checking for BSU authorisation...\n"
        if not self.mounted:
            return msg + "BSU not present. Insert BSU and retry."
        if not self.authed(self.mountpoint):
            return msg + "BSU not authorised. You must authorise your BSU before continuing."
        self.copy_file(self.order_file, os.path.join(self.mountpoint, self.order_file))
        self.monitor.unmount(self.mountpoint, True)
        return msg + """\
BSU authorised.
Retrieving orders from database...
Filtering orders requiring deficient skills:
    [SKL_INT, SKL_WRITE, SKL_SHOOT_STRAIGHT]
Order downloaded to BSU.
Ejecting BSU...
Your BSU is now safe to remove.
"""

    def cmd_rdms(self, params):
        """Usage: rdms [list|<id>]

rdms list   lists messages
rdms read <id>   displays message with corresponding id"""
        if not params:
            return 'rdms: insufficient parameters given'
        if params[0] == 'list':
            messages = ['  %d  %s  %s' % (i, msg.date, msg.subject) for i, msg in enumerate(self.messages)]
            return """\
You have %d messages available:
 ID  DATE           SUBJECT
""" % len(messages) + '\n'.join(messages)
        if params[0] == 'read':
            if len(params) == 1:
                return 'rdms: no msg id supplied'
            try:
                id = int(params[1])
                msg = self.messages[id]
            except (ValueError, IndexError):
                return 'rdms: invalid msg id: ' + params[1]
            return msg.body
        else:
            return 'rdms: did not understand command: ' + params[0]

    def cmd_bsuc(self, params):
        """Check BSU for problems."""
        if not self.mounted:
            return 'No BSU detected. Perform physical examination, turn upside down and reinsert. If that fails, turn upside down again and reinsert. If you have misplaced your BSU, request assistance at H38.'
        if not self.mountpoint:
            return 'Internal error. Try again. If problem persists, request assistance at H38.'

        msg = 'BSU found, running checks...\n'
        filenames = ('analysis', 'backup', 'ftl_sdr', 'maintenance', 'orders')
        for fn in filenames:
            if os.path.exists(os.path.join(self.mountpoint, fn)):
                return msg + 'BSU check revealed problems. Back up your BSU data, clear it, and retry. If problem persists, request assistance at H38.'
        msg += 'Integrity check: [OK]\n'
        if not os.access(self.mountpoint, os.W_OK):
            return msg + 'BSU check revealed problems. BSU data storage not writeable. Check BSU and try again.'
        msg += 'Write check: [OK]\n'

        self.bsu_checked = True
        return msg + 'BSU check passed.'

    def cmd_bsua(self, params):
        """(Re)-authorise a BSU."""
        if not self.mounted:
            return 'No BSU inserted.'
        if self.authed(self.mountpoint):
            return 'BSU already authorised.'
        if not self.bsu_checked:
            return 'BSU must be checked before it can be authorised.'
        self.copy_file(self.auth_path, os.path.join(self.mountpoint, self.auth_path))
        return 'Authorisation complete. Since your BSU is newly authorised it may take longer than usual to operate on terminals. Leave BSU in terminals for at least thirty seconds to ensure authorisation and operations complete before removal.'

    def cmd_test(self, params):
        return "Test passed."

    def cmd_schd(self, params):
        """Set scheduling information.
Usage: schd <cmd> [parameters...]

schd get           get current schedule
schd up <id>       move task <id> up
schd down <id>     move task <id> down
schd add <desc>    add task with given description
schd del <id>      del task <id>
schd take <id>     assign task <id> to yourself"""
        return "You do not have permission to access scheduling information."

    def cmd_reset(self, params):
        """admin"""
        self.reset()

    def cmd_reallyquit(self, params):
        """admin"""
        if self.mounted:
            self.monitor.unmount(self.mountpoint, True)
        self.application.exit()

    def cmd_quit(self, params):
        """Quit the shell"""
        self.reset()

    def done_dialog(self):
        self.body.floats.remove(self.dialog_float)
        get_app().layout.focus(self.cli.buffer)

    def new_mount(self, path):
        if self.mountpoint:
            return
        self.mountpoint = path

    def unmounted(self, path):
        self.bsu_checked = False
        self.mountpoint = None


class MyFrame(object):
    def __init__(self, body, title='', style='', width=None, height=None,
                 key_bindings=None, modal=False):
        assert is_container(body)
        assert is_formatted_text(title)
        assert is_dimension(width)
        assert is_dimension(height)
        assert key_bindings is None or isinstance(key_bindings, KeyBindings)
        assert isinstance(modal, bool)

        self.title = title
        self.body = body

        fill = partial(Window, style='class:frame.border')
        style = 'class:frame ' + style

        top_row_with_title = VSplit([
            fill(width=1, height=1, char=Border.TOP_LEFT),
            fill(width=1, height=1, char=Border.HORIZONTAL),
            #fill(width=1, height=1, char='|'),
            # Notice: we use `Template` here, because `self.title` can be an
            # `HTML` object for instance.
            Label(lambda: Template('{}').format(self.title),
                  style='class:frame.label',
                  dont_extend_width=True),
            #fill(width=1, height=1, char='|'),
            fill(char=Border.HORIZONTAL),
            fill(width=1, height=1, char=Border.TOP_RIGHT),
        ], height=1)

        top_row_without_title = VSplit([
            fill(width=1, height=1, char=Border.TOP_LEFT),
            fill(char=Border.HORIZONTAL),
            fill(width=1, height=1, char=Border.TOP_RIGHT),
        ], height=1)

        bottom_row = VSplit([
            fill(width=1, height=1, char=Border.BOTTOM_LEFT),
            fill(char=Border.HORIZONTAL),
            Label('IHunt OS v63.0', dont_extend_width=True),
            fill(width=1, height=1, char=Border.BOTTOM_RIGHT),
        ], height=1)

        @Condition
        def has_title():
            return bool(self.title)

        self.container = HSplit([
            ConditionalContainer(
                content=top_row_with_title,
                filter=has_title),
            ConditionalContainer(
                content=top_row_without_title,
                filter=~has_title),
            VSplit([
                fill(width=1, char=Border.VERTICAL),
                DynamicContainer(lambda: self.body),
                fill(width=1, char=Border.VERTICAL),
                    # Padding is required to make sure that if the content is
                    # too small, that the right frame border is still aligned.
            ], padding=0),
            bottom_row,
        ], width=width, height=height, style=style, key_bindings=key_bindings,
        modal=modal)

    def __pt_container__(self):
        return self.container



class PressAnyKey:
    def __init__(self, parent):
        self.parent = parent

        kb = KeyBindings()
        @kb.add('<any>')
        def _(event):
            self.handle_key(event)
            #self.body.text = str(event.data)
            #self.body.text = '\n'.join(dir(event))
        self.target_text = "any"
        self.received_text = ""
        self.instructions = (
                'Press any key',
                'Press ANY key',
                'No, press ANY key!',
                'Seriously though, *ANY*.',
                )
        self.instruction_index = 0
        self.body = Label(self.instructions[0])
        frame_body = HSplit([
            # Add optional padding around the body.
            Box(body=DynamicContainer(lambda: self.body),
                padding=D(preferred=1, max=1),
                padding_bottom=0),
            Window(FocusableControl())
        ])

        frame = Shadow(body=Frame(
            title=lambda: "Alert",
            body=frame_body,
            style='class:dialog.body',
            width=None,
            key_bindings=kb,
            modal=True,
            ))

        self.container = Box(
            body=frame,
            style='class:dialog',
            width=None)

    def reset(self):
        self.received_text = ""
        self.instruction_index = 0
        self.body.text = self.instructions[0]

    def handle_key(self, event):
        key = event.data.lower()
        idx = len(self.received_text)
        desired_key = self.target_text[idx]
        if key == desired_key:
            self.instruction_index = -1
            self.received_text = self.received_text + key
            if self.received_text == self.target_text:
                self.parent.done_dialog()
                return
            remaining = ' ' * (idx) + self.target_text[idx+1:]
            self.body.text = "Press %s key" % (remaining,)
        else:
            self.instruction_index += 1
            self.received_text = ""
            idx = min(self.instruction_index, len(self.instructions)-1)
            self.body.text = self.instructions[idx]

    def __pt_container__(self):
        return self.container


class FocusableControl(UIControl):
    def create_content(self, width, height):
        def get_line(i):
            return []

        return UIContent(
                get_line=get_line,
                line_count=100 ** 100)  # Something very big.

    def is_focusable(self):
        return True

class TakeBackup(PuzzleZeroStage, FileCopyStage):
    files = ('backup',)
    dest = 'backup'
    def __init__(self):
        super().__init__('P0.1: Take Backups')

    def new_mount(self, path):
        self.global_log(path, 'Accessing: Backup Interface Terminal 2')
        if self.authed(path):
            super().new_mount(path)
            self.global_log(path, 'Backup made into backup/')
        else:
            self.global_log(path, 'BSU not authorised. Go to ADMIN COORDINATION to authorise it.')

class UnplugMonitor(threading.Thread):
    def __init__(self, subsystem_filter, callback):
        super().__init__(daemon=False)
        self.subsystem_filter = subsystem_filter
        self.callback = callback
        self.start()

    def run(self):
        context = pyudev.Context()
        monitor = pyudev.Monitor.from_netlink(context)
        monitor.filter_by(subsystem=self.subsystem_filter)
        monitor.start()

        while True:
            r, w, x = select.select([monitor.fileno()], [], [])
            if monitor.fileno() in r:
                for udev in iter(partial(monitor.poll, 0), None):
                    if udev.device_node and udev.action == u'remove':
                        self.callback()
                        break

PUZZLES = {'1': ObtainOrders, '2': TakeBackup}

if __name__ == '__main__':
    pz_id = sys.argv[1]
    try:
        puzzle = PUZZLES[pz_id]
    except KeyError:
        print('argument should be one of %s' % ', '.join(PUZZLES.keys()))
        sys.exit(1)
    puzzle()
