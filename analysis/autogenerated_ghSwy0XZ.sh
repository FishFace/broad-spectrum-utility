#!/bin/sh

# AUTO-GENERATED SCRIPT
# GENERATED BY SCIENCE LAB ADVANCED ANALYSIS COMPUTER
#
# Target: FTL Maintenance Computer
# Purpose: Fix spooler and core misalignment caused by excessive smut
# Description: Script will recalibrate spooler alignment and core alignment in
# sequence. Both the primary and tertiary relay circuits will be bypassed and
# the over-spool detector coefficients will be recomputed. All non-essential
# personnel should be evacuated from the areas surrounding the FTL drive sinks.
# This process may take some time during which FTL drive will be inoperative.
# Ink level monitoring may be inaccurate for up to one hour.

### BEGIN AUTHORISATION CODE ###
# [to be filled in by technician]
###  END AUTHORISATION CODE  ###

init 6
