#!/bin/bash

run_puzzle () {
	if [ "$1" = "0" ]; then
		if [ "$2" = "1" ]; then
			# An admin needs to start 0.1 anyway.
			exit 0
		fi
		script="./puzzle_zero.py"
	elif [ "$1" = "1" ]; then
		script="./puzzle_one.py"
	else
		echo "$1 is not a valid puzzle number"
		exit 1
	fi
	
	. venv3.5/bin/activate
	$script $2 &
}
puzzle=`cat PUZZLE`
run_puzzle $puzzle
