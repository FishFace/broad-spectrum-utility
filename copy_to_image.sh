#!/bin/bash

sudo rsync --delete --chown=1001:1001 --exclude=rootfs --exclude=boot --exclude=image.img --exclude=*.swp --exclude=puzzle.log -av * rootfs/home/ihunt/usb/
if [ \( -n "$1" \) -a \( -n "$2" \) ]; then
	sudo sh -c "echo $1 $2 > rootfs/home/ihunt/usb/PUZZLE"
fi
