import io

def open(fd, mode='r'):
    if 'b' in mode:
        return io.open(fd, mode)
    else:
        return io.open(fd, mode, newline='\r\n')

FUTURE = 128055 * 365 * 24 * 60 * 60
def stardate(dt):
    ts = int(dt.timestamp()) + FUTURE
    return str(ts)
    #us = dt.microseconds
    #return '%d.%04d' % (ts, us)
