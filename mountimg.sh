#!/bin/bash

los() (
	img="$1"
	dev="$(sudo losetup --show -f -P "$img")"
	if [ -z "$dev" ]; then
		echo "Not an image file"
		exit 1
	fi
	echo "Mounting using $dev"
	echo "Waiting for labels to appear"
	sleep 1
	for part in "$dev"?*; do
		if [ "$part" = "${dev}p*" ]; then
			part="${dev}"
		fi
		name="$(lsblk -o label --raw "$part" | tail -1)"
		dst="$name"
		echo "Mounting $part at $dst"
		sudo mkdir -p "$dst"
		sudo mount -o rw "$part" "$dst"
	done
	if [ -d rootfs ]; then
		echo "Preparing rootfs"
		sudo cp /usr/bin/qemu-arm-static rootfs/usr/bin
		sudo mount --rbind /dev rootfs/dev
		sudo mount --make-rslave rootfs/dev
		sudo mount -t proc none rootfs/proc
		sudo mount -o bind /sys rootfs/sys
	fi
)
losd() (
	dev="/dev/loop$1"
	if [ -d rootfs ]; then
		echo "Cleaning up rootfs"
		sudo rm rootfs/usr/bin/qemu-arm-static
		sudo umount -R rootfs/dev
		sudo umount rootfs/proc
		sudo umount rootfs/sys
	fi
	for part in "$dev"?*; do
		if [ "$part" = "${dev}p*" ]; then
			part="${dev}"
		fi
		name="$(lsblk -o label --raw "$part" | tail -1)"
		dst="$name"
		sudo umount "$dst"
	done
	sudo losetup -d "$dev"
)

if [[ "$0" == *umountimg.sh ]]; then
	losd 0
elif [[ "$0" == *mountimg.sh ]]; then
	los $1
fi
