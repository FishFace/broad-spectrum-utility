import threading
import select
import subprocess
import logging
import os
import shutil
from distutils.dir_util import copy_tree
from datetime import datetime, timedelta, timezone

from util import stardate, open

DEVNULL = subprocess.DEVNULL
DEVICES = (b'/dev/sda',)
STORY_DRIVE_PATH = '/mnt/bsu/001/'

# The above are for the pis, the following is for us!
debug_log_file = 'puzzle.log'
logging.basicConfig(filename=debug_log_file, format='%(asctime)s %(levelname)s:%(name)s:%(message)s', datefmt='%F %H:%M:%S', level=logging.INFO)

tz = timezone(timedelta(hours=1)) # UTC+1 = BST = LAN time



class PuzzleStage(object):
    error_file = 'IHUNT_ERROR'
    error_file_contents = 'This file should not be here; it means there was a problem with your USB stick or with the puzzle. It might be impossible to continue! Check the USB stick has enough space, try again --- and wait 30 seconds to make sure before removing --- and if this file is still here, contact the hunt admins.'
    log_file = None
    global_log_file = 'bsu.log'
    unmount_immediately = True

    def __init__(self, name):
        self.name = name
        self.monitor = MountMonitor.register(self)
        self.logger = logging.getLogger(name)
        self.logger.info('Puzzle started')
        self.mounted = False

    def defunct(self, path):
        """Return True if the stage should not run because the player is past it."""
        return False

    def copy_files(self, files, dest):
        for file in files:
            self.copy_file(file, dest)

    def copy_file(self, file, dest):
        if os.path.isdir(file):
            subprocess.check_call(['cp', '-rT', file, dest])
        else:
            shutil.copyfile(file, dest)

    def create_file(self, path, contents):
        """Create a new file at `path` containing `contents` and flush the file."""
        dir = os.path.dirname(path)
        if not os.path.isdir(dir):
            try:
                subprocess.check_call(['mkdir', '-p', dir], stdout=DEVNULL, stderr=DEVNULL)
            except subprocess.CalledProcessError:
                self.logger.warn('Could not create %s for logging (it probably already exists)', path)
        self.logger.debug('Creating file at %s and writing %d bytes', path, len(contents))
        fd = open(path, 'w')
        fd.write(contents)
        fd.flush()
        os.fsync(fd)
        fd.close()

    def info(self, path, msg):
        """Write the msg to the puzzle log file. Path is the mountpoint, self.log_file is the path to the file itself."""
        if self.log_file is None:
            raise ValueError('Log file must be defined')

        fullpath = os.path.join(path, self.log_file)
        with open(fullpath, 'a') as fd:
            fd.write(msg)
            fd.flush()
            os.fsync(fd)

    def global_log(self, path, msg):
        """Write msg to the global log file"""
        fullpath = os.path.join(path, self.global_log_file)
        with open(fullpath, 'a') as fd:
            for line in msg.split('\n'):
                fd.write('[%s] %s\n' % (stardate(datetime.now(tz=tz)), line))
            fd.flush()
            os.fsync(fd)

    def check_send_log(self, path):
        if os.path.isfile(os.path.join(path, 'IHUNT_LOG_PLZ')):
            self.logger.info('Found log request file, downloading log')
            self.copy_file(debug_log_file, os.path.join(path, '%s.log' % self.name[:4]))

    def _new_mount(self, path):
        """Called when a new mount is detected. Calls new_mount, but creates a guard file in case of errors."""
        self.mounted = True

        if self.defunct(path):
            self.logger.info('Not running due to being defunct')
            return

        ef_path = os.path.join(path, self.error_file)
        try:
            self.create_file(ef_path, self.error_file_contents)
            if self.log_file is not None:
                self.create_file(os.path.join(path, self.log_file), '')
        except OSError as e:
            self.logger.error('Error creating standard files in new mount: ' + str(e))
        self.check_send_log(path)
        self.logger.debug('Callback triggered for new mount at %s', path)
        self.new_mount(path)
        try:
            subprocess.check_call(['rm', ef_path], stdout=DEVNULL, stderr=DEVNULL)
        except subprocess.CalledProcessError as e:
            self.logger.error('Could not delete error guard file: ' + str(e))

    def new_mount(self, path):
        """Will be called with USB mount paths"""
        pass

    def _unmounted(self, path):
        self.mounted = False
        self.unmounted(path)

    def unmounted(self, path):
        pass

    def __str__(self):
        return self.name

class FileCopyStage(PuzzleStage):
    files = ()
    dest = ''

    def new_mount(self, path):
        super().new_mount(path)
        self.logger.debug(path)

        dest = os.path.join(path, self.dest)
        self.logger.info('Copying %d files to %s', len(self.files), dest)
        self.copy_files(self.files, dest)

class MountMonitor(threading.Thread):
    callbacks = []
    instance = None
    def __init__(self):
        super().__init__(daemon=False)
        self.old_mounts = []
        self.start()

    @classmethod
    def register(cls, puzzle):
        """Register a callback on a puzzle to be called when a USB drive is mounted. Takes one argument, the mountpoint."""
        cls.callbacks.append(puzzle)
        if cls.instance is None:
            cls.instance = cls()

        return cls.instance

    @classmethod
    def deregister(cls, puzzle, callback):
        cls.callbacks.remove((puzzle, callback))
    
    def run(self):
        """Check for new mounts and call get_mounts when there might be some"""
        fd = open('/proc/self/mounts', 'rb')

        # Run once when starting in case there's already a drive attached
        self.get_mounts(fd)

        p = select.poll()
        p.register(fd, select.POLLERR | select.POLLPRI)
        while True:
            events = p.poll(10 * 1000)
            if events:
                fd.seek(0)
                self.get_mounts(fd)

    def get_mounts(self, fd):
        """Scan for mounts matching what might be USB drives"""
        mounts = fd.readlines(1000*1000)
        good_mounts = []
        for mount in mounts:
            if mount.startswith(DEVICES):
                mountpoint = mount.split()[1].decode('unicode-escape')
                self.found_mount(mountpoint)
                good_mounts.append(mount)

        for mount in self.old_mounts:
            if mount not in good_mounts:
                mountpoint = mount.split()[1].decode('unicode-escape')
                self.unmounted(mountpoint)

        self.old_mounts = good_mounts

    def found_mount(self, mountpoint):
        """Call all registered callbacks on the mountpoint"""
        logging.info('New mount found at %s', mountpoint)
        unmount_immediately=True
        for puzzle in self.callbacks:
            unmount_immediately &= puzzle.unmount_immediately
            try:
                puzzle._new_mount(mountpoint)
            except:
                puzzle.logger.exception('Error in mount callback')

        self.unmount(mountpoint, unmount_immediately)

    def unmounted(self, mountpoint):
        logging.info('Mount disappeared from %s', mountpoint)
        for puzzle in self.callbacks:
            try:
                puzzle._unmounted(mountpoint)
            except:
                puzzle.logger.exception('Error in unmount callback')

    def unmount(self, mountpoint, unmount_immediately=True):
        os.sync()
        if unmount_immediately:
            try:
                subprocess.check_call(['sudo', 'umount', '-f', mountpoint], stdout=DEVNULL, stderr=DEVNULL)
            except subprocess.CalledProcessError as e:
                logging.warning('Failed to unmount %s. Exit stats: %d', mountpoint, e.returncode)

#years
