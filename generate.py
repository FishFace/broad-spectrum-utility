#!/usr/bin/env python3

import datetime
import random
import os
from base64 import b64encode
from util import stardate, open
from numpy import clip
from collections import defaultdict
from math import exp, sqrt, atan, degrees, sin, cos, radians
import subprocess
from glob import glob

def ftl_data():
    start = datetime.datetime(2018, 8, 19)
    for i in range(7):
        with open('ftl_sdr/ftl_splr.%02d.dat' % i, 'w') as fd:
            now = datetime.datetime(2018, 8, i+8)
            fd.write("""\
+++ PRIMARY FTL SPOOLER +++
    DATA LOG
    Legend:
        J = Jump register contents
        S = Spooler data
        P = Paper remaining
    Information:
        All entries normalised
        For further analysis bring to science lab
    Log starts:
        %s

""" % stardate(start))
            for j in range(24 * 60):
                J = ' '.join(('%02d' % random.randrange(100) for _ in range(28)))
                S = ' '.join(('%02d' % random.randrange(100) for _ in range(28)))
                P = ' '.join(('%02d' % random.randrange(100) for _ in range(28)))
                fd.write("""\
J: %s
S: %s
P: %s

""" % (J, S, P))
            fd.write("""\
    Log complete
+++ end of line +++
""")
    with open('ftl_sdr/ftl_coil_temp.dat', 'w') as fd:
        fd.write("""\
+++ PRIMARY FTL SPOOLER +++
    COIL TEMPERATURE LOG
    Legend:
        Stardate
        Mean
        Maximum
        Minimum
        Standard deviation
        (All values Kelvin)
    Log starts:
        %s
+++ LOG +++
""" % (stardate(start),))
        for day in range(7):
            temp = random.normalvariate(90, 3)
            tmax = temp + random.normalvariate(2, 0.5)
            tmin = temp - random.normalvariate(1, 0.2)
            sd = random.normalvariate(1, 0.1)
            fd.write('    %s: %4.1f (%4.1f/%4.1f/%4.2f)\n' % (stardate(start + datetime.timedelta(days=day)), temp, tmax, tmin, sd))
        fd.write('+++ end of line +++\n')
    with open('ftl_sdr/ftl_engine_intf.dat', 'w') as fd:
        fd.write("""\
+++ PRIMARY FTL SPOOLER +++
    FTL ENGINE INTERFACE LOG
    Log starts:
        %s
+++ LOG +++
    [No interface use recorded during log period]
+++ end of line +++
""" % stardate(start))
    with open('ftl_sdr/ftl_paper_supply.dat', 'w') as fd:
        fd.write("""\
+++ PRIMARY FTL SPOOLER +++
    FTL PAPER SUPPLY DATA
    Sizes supported:
        A5 (corners removed)
        A4 (corners removed)
        A3 (corners removed)
        A2 (corners removed)
        US Letter (corners removed)
        US Legal (corners removed)
    Supply remaining (resp):
        69300
        0
        51452
        782819
        639259
        896059
    Waste corner tray:
        0.5% capacity remaining
+++ end of line +++
""")

def nav():
    start = datetime.datetime(2017, 8, 27)
    end = datetime.datetime(2018, 8, 23)
    dates = []
    date = start
    while date < end:
        dates.append(date)
        seconds = clip(random.normalvariate(6*24*60*60, 1*24*60*60), 33*60, 14*24*60*60)
        date += datetime.timedelta(seconds=seconds)
    
    a1 = random.randrange(0, 360)
    a2 = random.randrange(0, 360)

    with open(os.path.join('backup', 'nav', 'jumps.log'), 'w') as fd:
        fd.write("""\
+++ NAVIGATION COMPUTER +++
    FTL COORDINATE LOG
    Log starts:
        %s
    Log ends:
        %s
    Total jumps:
        %d
    Legend:
        Phi, Theta (degrees, relative to standard star reference)
        Distance (ly)
+++ LOG +++
""" % (stardate(start), stardate(dates[-1]), len(dates)))
        for d in dates:
            aa1 = random.normalvariate(a1, a1*0.01) % 360
            aa2 = random.normalvariate(a2, a2*0.01) % 360
            distance = max(random.normalvariate(15, 1), 0)
            fd.write("""\
--- Jump at %s ---
    Phi:      %.18f
    Theta:    %.18f
    Distance: %.18f
""" % (stardate(d), aa1, aa2, distance))

    astro(dates, a1, a2)
    spectrograph(dates)

def weapons():
    start = datetime.datetime(2018, 4, 15)
    dates = [] #[start + datetime.timedelta(days=i) for i in range(120)]
    intensities = {}
    max_seconds = 126*24*60*60
    for i in range(120):
        s = random.randrange(max_seconds)
        date = start + datetime.timedelta(seconds=s)
        intensity = max(random.normalvariate(180, 70),0)
        dates.append(date)
        intensities[date] = intensity
    dates.sort()

    main_batteries(start, dates, intensities)
    pd(start, dates, intensities)

    # Not weapons but we use the data to make it match
    dradis(dates, intensities)

def main_batteries(start, dates, intensities):
    shots = defaultdict(lambda: 0)

    for i in range(6):
        for surface in ('D', 'V'):
            for side in ('P', 'S'):
                id = '%s%d%s' % (surface, i, side)
                path = os.path.join('backup', 'weapons', 'batteries', id)
                accuracy = clip(random.normalvariate(.9, .1), 0, .99)
                speed = clip(random.normalvariate(45,1), 0, 180)
                with open(path, 'w') as fd:
                    fd.write("""\
+++ MAIN BATTERY LOG +++
    BATTERY %s
    Legend:
        Stardate
        Mode (F=Flak, O=Offensive)
        Shots fired
        Accuracy
        Tracking speed (avg, deg/sec)
+++ LOG +++
""" % (id))
                    for d in dates:
                        intensity = intensities[d]
                        m = random.choices('FO', (.8, .2))[0]
                        if m == 'O':
                            intensity *= .05
                        s = int(max(random.normalvariate(intensity, intensity*0.1), 0))
                        shots[d] += s
                        a = 100*int(clip(random.normalvariate(accuracy*s, 0.03*s), 0, s))/s if s > 0 else 0
                        t = max(random.normalvariate(speed, 5), 0)
                        fd.write("""\
%s %s; %3d; %6.02f%% %4.01f
""" % (stardate(d), m, s, a, t))

                    fd.write("""\
+++ end of line +++
""")

    ammo_log('MAIN BATTERY LOG', os.path.join('backup', 'weapons', 'batteries'), start, shots)

def pd(start, dates, intensities):
    shots = defaultdict(lambda: 0)
    with open(os.path.join('backup', 'weapons', 'ciws', 'perf.log'), 'w') as fd:
        fd.write("""\
+++ CLOSE-IN WEAPONS SYSTEMS LOG +++
    AGGREGATE PERFORMANCE DATA
    Legend:
        Stardate
        Firing pattern
        ROF (avg, rpm)
        Shots fired
        Accuracy (estimated, 1=best, 5=worst)
+++ LOG +++
""")
        for d in dates:
            intensity = intensities[d]
            m = random.choice(('Opt','Adp','Opp','Con'))
            r = clip(random.normalvariate(intensity/130 * 450, 100), 0, 1200)
            s = int(max(random.normalvariate(intensity, intensity*0.2), 2/r) * r) if r > 0 else 0
            shots[d] = s
            a = random.choices('12345', (1,2,3,6,3))[0]
            fd.write("""\
%s %s; %6.1f; %6d %s
""" % (stardate(d), m, r, s, a))

        fd.write("""\
+++ end of line +++
""")

    ammo_log('CLOSE-IN WEAPONS SYSTEMS LOG', os.path.join('backup', 'weapons', 'ciws'), start, shots)

def ammo_log(name, path, start, shots):
    shots_iter = iter(shots.items())
    shots_cum_week = {}
    shot_to_date = 0
    date = start
    for day in range(7, 127, 7):
        to_date = start + datetime.timedelta(days=day)
        while date < to_date:
            try:
                date, shot = next(shots_iter)
            except StopIteration:
                break
            shot_to_date += shot
        shots_cum_week[to_date] = shot_to_date

    initial_ammo = shot_to_date * random.uniform(3,8)
    with open(os.path.join(path, 'stores'), 'w') as fd:
        fd.write("""\
+++ %s +++
    AMMUNITION STORES
    Log starts:
        %s
    Log ends:
        %s
    Initial amount:
        %d
    Final amount:
        %d
    Consumed during period:
        %d
    Legend:
        Stardate
        Ammunition remaining
+++ LOG +++
""" % (name, stardate(start), stardate(to_date), initial_ammo, initial_ammo - shot_to_date, shot_to_date))
        for date, shots in shots_cum_week.items():
            fd.write("""\
%s: %d
""" % (stardate(date), initial_ammo - shots))
        fd.write("""\
+++ end of line +++
""")

def dradis(dates, intensities):
    with open(os.path.join('backup', 'sensors', 'DRADIS', 'contacts.log'), 'w') as fd:
        fd.write("""\
+++ SENSOR AGGREGATION COMPUTER +++
    DRADIS CONTACT LOG
    Legend:
        Stardate
        Number of contacts
+++ FILTER SETTINGS +++
    Filtering 31 ships with colonial transponders:
""")
        for i in range(31):
            iff = b64encode(open('/dev/urandom', 'rb').read(9)).decode('ascii')
            fd.write('        %s\n' % iff)
        fd.write('+++ LOG +++\n')
        for d in dates:
            intensity = intensities[d]
            contacts = max(int(intensity ** 1.5 / 4), 2)
            fd.write('    %s: %4d\n' % (stardate(d), contacts))
        fd.write('+++ end of line +++')
    with open(os.path.join('backup', 'sensors', 'DRADIS', 'calibration.dat'), 'w') as fd:
        start = datetime.datetime(2018, 8, 15)
        end = start + datetime.timedelta(days=30)
        fd.write("""\
+++ SENSOR AGGREGATION COMPUTER +++
    DRADIS CALIBRATION INFORMATION
    Calibrated:
        %s
    Next calibration due:
        %s
    Synchrotron frequency offset:
        0.45 THz
    Clock slew:
        0.018 ns
    Beamforming parameters:
        9.02, 0.044, 0.575, 0.564
    Phase deconflicting factor:
        0.66
    Gyroscopic correction:
        0.02, 1.04, 0.39 milliradians
    Wallhacks:
        enabled
+++ end of line +++
""" % (stardate(start), stardate(end)))

def chem():
    start = datetime.datetime(2017, 9, 1)
    end = datetime.datetime(2018, 8, 1)
    date = start
    while date < end:
        sd = stardate(date)
        with open(os.path.join('backup', 'sensors', 'chem', '%s.sns' % sd), 'w') as fd:
            atoms = random.randrange(1000, 10e8)
            fd.write("""\
+++ SENSOR AGGREGATION COMPUTER +++
    INTERSTELLAR MATERIAL COMPOSITION
    Data gathered:
        %s
    Ions sampled:
        %d
    Legend:
        Atomic number: percentage
+++ CHEMICAL ANALYSIS +++
""" % (sd, atoms))

            if date == datetime.datetime(2018, 2, 17):
                #Supernovas
                results = {
                        16: .75 * random.normalvariate(1, .1),
                        92: .25 * random.normalvariate(1, .1),
                        15: .05 * random.normalvariate(1, .1),
                        68: .01 * random.normalvariate(1, .1),
                        102: 0.005 * random.normalvariate(1, .1),
                        23: 0.001 * random.normalvariate(1, .1),
                        33: 0.0005 * random.normalvariate(1, .1)
                }
                total = sum(results.values())
                output = [key for key, _ in sorted(results.items(), key=lambda x: (x[1], x[0]), reverse=True)]
                if output != [16, 92, 15, 68, 102, 23, 33]:
                    print("RE-RUN BECAUSE THE ORDER IS BROKEN!")
            elif random.randint(0, 30):
                # Standard distribution
                results = {1: .910 * random.normalvariate(1, .02), 2: .089 * random.normalvariate(1, 0.005)}
                for i in range(random.randint(0, 6)):
                    number = random.randint(3, 82)
                    results[number] = random.uniform(0, 0.001)
                total = sum(results.values())
            else:
                results = {1: .75 * random.normalvariate(1, .1), 2: .15 * random.normalvariate(1, 0.01)}
                for i in range(random.randint(8,16)):
                    number = random.randint(3, 82)
                    results[number] = random.uniform(0, 0.005)
                total = sum(results.values())

            for chem in sorted(results.keys()):
                amt = results[chem]
                fd.write('    %2d: %5.2f%%\n' % (chem, amt/total*100))
            fd.write('+++ end of line +++\n')
        date += datetime.timedelta(days=1)

def astro(dates, phi, theta):
    subprocess.check_call(['rm'] + glob('backup/sensors/astro/*'))
    n = random.randint(50,500)
    for i in range(n):
        period = pow(10, random.uniform(-4, 0.175))
        fn = '%d_%d.log' % (random.randrange(1000, 10000), period*1e4)
        width = clip(random.normalvariate(100,50), 1, 850)
        x = clip(random.normalvariate(0, 150), -1000, 1000)
        y = clip(random.normalvariate(0, 150), -1000, 1000)
        z = clip(random.normalvariate(0, 150), -1000, 1000)
        #dist = random.uniform(100, 1000)
        peak = random.normalvariate(425, 25)
        with open(os.path.join('backup', 'sensors', 'astro', fn), 'w') as fd:
            fd.write("""\
+++ ASTRONAVIGATIONAL SENSORS +++
    PULSAR LOG
    Pulsar ID:
        %s
    Period:
        %5.4f s
    Pulse width:
        %3.0f mP
    Peak frequency:
        %3.0f mHz
+++ SIGHTINGS +++
""" % (fn, period, width, peak))
            sightings = random.choices((1, 2, 3, 4, 5), weights=(1, 10, 5, 2, 1))[0]
            initial = random.randrange(len(dates))
            for d in dates[initial:initial+sightings]:
                delay = datetime.timedelta(seconds=random.randrange(200, 1000))
                sd = stardate(d+delay)
                p_phi = degrees(atan(y/x))
                p_theta = degrees(atan(z/x))
                strength = 4000000/(x**2 + y**2 + z**2)
                x -= cos(radians(phi))*15
                y -= sin(radians(phi))*15
                z -= sin(radians(theta))*15
                fd.write("""\
--- Date: %s ---
    Direction:
        %6.2f, %6.2f degrees
    Amplitude:
        %4.0f mJ
""" % (sd, p_phi, p_theta, strength))
            fd.write('+++ end of line +++\n')

ranges = {
    'UV': (10, 300),
    'Visible': (300, 700),
    #'IR': (700, 10000)
}
def spectrograph(dates):
    subprocess.call(['rm'] + glob('backup/sensors/spectra/stellar/*'))
    subprocess.call(['rm'] + glob('backup/sensors/spectra/planetary/*'))

    star_dates = dates[:]
    random.shuffle(star_dates)
    n_stars = random.randint(10, 50)
    star_dates = dates[:n_stars]

    planet_dates = star_dates[:]
    random.shuffle(planet_dates)
    n_planets = random.randint(int(n_stars/10), int(n_stars/2))
    planet_dates = planet_dates[:n_planets]
    target_planet = 9
    while True:
        planet_numbers = {d: random.randint(1,8) for d in planet_dates}
        if sum(planet_numbers.values()) > target_planet:
            break

    p_idx = 0
    for d in star_dates:
        if d in planet_dates and p_idx < target_planet and p_idx + planet_numbers[d] >= target_planet:
            id = 'ZGF5YnJlYWs=' #daybreak
            print('star scanned', stardate(d))
        else:
            id = b64encode(open('/dev/urandom', 'rb').read(6)).decode('ascii')
        with open('backup/sensors/spectra/stellar/%s.spc' % stardate(d), 'w') as fd:
            fd.write("""\
+++ SENSOR AGGREGATION COMPUTER +++
    SPECTROGRAPH
    Type:
        Absorption, stellar
    ID:
        %s
    Date:
        %s
    Unit:
        nm
+++ PEAKS +++
""" % (id, stardate(d)))
            for type, (l_min, l_max) in ranges.items():
                fd.write('    %s:\n        ' % type)
                n_lines = random.randint(50, 150)
                lines = sorted([random.uniform(l_min, l_max) for _ in range(n_lines)])
                fd.write(', '.join('%.2f' % l for l in lines))
                fd.write('\n')
            fd.write('+++ end of line +++\n')

        if d in planet_dates:
            for i in range(planet_numbers[d]):
                p_idx += 1
                planet(d, id, i)
                if p_idx == target_planet:
                    print('special planet on: %s-%d' % (stardate(d), i))

def planet(date, id, i, special=False):
    planet = chr(98 + i)
    p_id = '%s %s' % (id, planet)
    with open('backup/sensors/spectra/planetary/%s-%s.spc' % (stardate(date), planet), 'w') as fd:
        fd.write("""\
+++ SENSOR AGGREGATION COMPUTER +++
SPECTROGRAPH
    Type:
        Absorption, planetary
    ID:
        %s
    Date:
        %s
    Unit:
        nm
+++ PEAKS +++
""" % (p_id, stardate(date)))
        for type, (l_min, l_max) in ranges.items():
            fd.write('    %s:\n        ' % type)
            n_lines = random.randint(50, 150)
            lines = sorted([random.uniform(l_min, l_max) for _ in range(n_lines)])
            fd.write(', '.join('%.2f' % l for l in lines))
            fd.write('\n')
        fd.write('+++ end of line +++\n')

if __name__ == '__main__':
    nav()
