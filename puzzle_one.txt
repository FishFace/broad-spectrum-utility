site: hint at location of first and second boxes, talk about obtaining backups/analysis
first box: somehow teams have to think to plug a USB drive in. What if they don't have one?
	on plugging something in it should get a load of data written to it, including a map which has the other locations
on (also for puzzle 2)
	it could also have information on how to perform the fixes required by the third (and maybe fourth) boxes before you
get the info, so if the team reads all the info in the data dump they won't have to do all the round-trips. (But most
probably won't realise initially)
second box:
	stick gets some files along with instructions telling people to bring it to the next location (which they can map to
a real location using the info from box 1)
third box:
	requires that the info from box 2 is in the right place (which it won't be initially) - they will need to go back to
their desk, fix it, then go back to the box. It will then give them something to bring to box 4
final box:
	maybe again make them fix something (go back to desk) with the file from box 3, and then give them something that
they can get the answer from (use the data from box 1)

desk --> first box
     --> desk (get second loc)
     --> second box
     --> desk (get third loc)
     --> third box
     --> desk (read error and move files)
     --> third box
     --> desk (get fourth loc)
     --> fourth box
     ?-> desk (fix script from third)
     ?-> fourth box
     --> desk (win?)


